# Private Keys

## WARNING

This is sensitive material! Keys are stored encrypted with PGP *only*. Running
these files through gnupg will result in a **cleartext private key**, which
should **never be written to disk**.

## Usage

### Generate a new RSA key

The second openssl command adds some metadata, which can be convinent for
building files that need a certificate and key.

```bash
    openssl genrsa 2048 \
      | openssl rsa -modulus \
      | gpg -ear <recp1> -r <recp2> ... -o <CN.rsa.asc>
```

### Reencrypting

It can be useful to save a key encrypted with a passphrase instead of PGP, as
this is something network equipment can understand. It is best practice to
generate a one-time password for this. Alternatively, you can make one up. Just
don't forget it, as you will need it when you upload the key. Note that writing
this password in cleartext to disk is equivalent to writing the cleartext
private key to disk.

First, generate a temporary passphrase to use:

```bash
pwgen 32 1 | gpg -eao pwfile.asc
```

Then, use this passphrase to reencrypt the file:

```bash
gpg -qd <keyfile>.asc \
  | openssl rsa \
    -inform PEM \
    -outform PEM \
    -passout file:<(gpg -qd < pwfile.asc) \
    -aes256
```

If you do not specify the `-passout` option, then you will be prompted for a
password.
