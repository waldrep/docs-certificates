# Certificate Signing Requests

The main command for working with requests is `openssl req`.
For advanced usage, read [`man 1 req`][man_req].

## Generate a new CSR manually

Prerequisites:
* A private [key][key].
* (optional) A [config][conf] file

Common arguments:
* `-new`: specifies that you are creating a new request.
* `-config <config-file>`: specify a config file.
  If you omit this, you will be prompted for essential metadata.
* `-key <key-file>`: specify the private key to use

Also see the [openssl idioms][idioms].

## Examples

Create a request with a configuration file.
```bash
openssl req \
  -config example.conf \
  -new \
  -key example.rsa \
  -out example.csr
```

Create a CSR with prompts:
```
$ openssl req -new -key example.key -out example.csr
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:US
State or Province Name (full name) [Some-State]:Virginia
Locality Name (eg, city) []:Blacksburg
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Virginia Polytechnic Institute and State University
Organizational Unit Name (eg, section) []:Secure Identity Services
Common Name (e.g. server FQDN or YOUR name) []:example.nis.vt.edu
Email Address []:nis.fn.certs-g@vt.edu

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:
$ cat example.csr
-----BEGIN CERTIFICATE REQUEST-----
MIIDHzCCAgcCAQAwgdkxCzAJBgNVBAYTAlVTMREwDwYDVQQIDAhWaXJnaW5pYTET
MBEGA1UEBwwKQmxhY2tzYnVyZzE8MDoGA1UECgwzVmlyZ2luaWEgUG9seXRlY2hu
aWMgSW5zdGl0dXRlIGFuZCBTdGF0ZSBVbml2ZXJzaXR5MSEwHwYDVQQLDBhTZWN1
cmUgSWRlbnRpdHkgU2VydmljZXMxGzAZBgNVBAMMEmV4YW1wbGUubmlzLnZ0LmVk
dTEkMCIGCSqGSIb3DQEJARYVbmlzLmZuLmNlcnRzLWdAdnQuZWR1MIIBIjANBgkq
hkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxZU9WhZAgzHuOzR7GEsllGh0we236OpY
ZJOLAWmbZDw93WRtmQgGU0SClkPUklyLW8IwNfWw7bmTT6MkpbmFryjWZ9wAHwAl
EFuDcjm2D4XXRoNAgmb4c7121s387je7bTM+SKAKrA8Cm2ljYBpyswwKEve0HdTM
dndQXq7GguUZ8C5COa+FE6R+aJSriUrYP7jFGYmLyN+JVIMZCFN9cJ7n+konuwiY
dUIIWXkE5m4V1i3shxcXcFTMwkAaW0ZV4dFPi6xt0Is5zsNLLKCjX7XqaXZrGJt/
B9p7Pp2/4id+kkZ3FL6NOkz3nAgea6DqVglLs40ut1QtMi/m18PvnQIDAQABoAAw
DQYJKoZIhvcNAQELBQADggEBAK4MTJ1uYl8Vvl9ZPOWGfXS0xNec8XLLlbuM5mUu
RZPXJCZWYh8RXZoKwvloFa9fpnInw3Z8i/PqL6ni7gEkU331GiZzFJti64c6hJ6F
hb1kMFuZK1SUMN8AP766+btsLbjoP4s/DuNItgbZ4AHnfZDb5dlCZlXDV6ibrZCQ
oQ86eoLzPi9MSV3qSaOAN0d9KORi6pnwarBFCG8CHTxN1//aJJWBTA6elg827D3P
fjU3l6ir7BNWzfzJn9DA1OfOdcLrsHGyUMzOfwVqhdwEEx33XbxKWKSOmb13amKt
HQsAcJPpNIDT3Upthy9agP6qUisQEOW3Dv/a7cLyI04dqjs=
-----END CERTIFICATE REQUEST-----
```

Look at an existing CSR:
```bash
openssl req \
  -in example.csr \
  -noout \
  -text
```

```
$ openssl req -in example.csr -noout -text
Certificate Request:
    Data:
        Version: 1 (0x0)
        Subject: C = US, ST = Virginia, L = Blacksburg, O = Virginia Polytechnic Institute and State University, OU = Secure Identity Services, CN = example.nis.vt.edu, emailAddress = nis.fn.certs-g@vt.edu
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:c5:95:3d:5a:16:40:83:31:ee:3b:34:7b:18:4b:
                    25:94:68:74:c1:ed:b7:e8:ea:58:64:93:8b:01:69:
                    9b:64:3c:3d:dd:64:6d:99:08:06:53:44:82:96:43:
                    d4:92:5c:8b:5b:c2:30:35:f5:b0:ed:b9:93:4f:a3:
                    24:a5:b9:85:af:28:d6:67:dc:00:1f:00:25:10:5b:
                    83:72:39:b6:0f:85:d7:46:83:40:82:66:f8:73:bd:
                    76:d6:cd:fc:ee:37:bb:6d:33:3e:48:a0:0a:ac:0f:
                    02:9b:69:63:60:1a:72:b3:0c:0a:12:f7:b4:1d:d4:
                    cc:76:77:50:5e:ae:c6:82:e5:19:f0:2e:42:39:af:
                    85:13:a4:7e:68:94:ab:89:4a:d8:3f:b8:c5:19:89:
                    8b:c8:df:89:54:83:19:08:53:7d:70:9e:e7:fa:4a:
                    27:bb:08:98:75:42:08:59:79:04:e6:6e:15:d6:2d:
                    ec:87:17:17:70:54:cc:c2:40:1a:5b:46:55:e1:d1:
                    4f:8b:ac:6d:d0:8b:39:ce:c3:4b:2c:a0:a3:5f:b5:
                    ea:69:76:6b:18:9b:7f:07:da:7b:3e:9d:bf:e2:27:
                    7e:92:46:77:14:be:8d:3a:4c:f7:9c:08:1e:6b:a0:
                    ea:56:09:4b:b3:8d:2e:b7:54:2d:32:2f:e6:d7:c3:
                    ef:9d
                Exponent: 65537 (0x10001)
        Attributes:
            a0:00
    Signature Algorithm: sha256WithRSAEncryption
         ae:0c:4c:9d:6e:62:5f:15:be:5f:59:3c:e5:86:7d:74:b4:c4:
         d7:9c:f1:72:cb:95:bb:8c:e6:65:2e:45:93:d7:24:26:56:62:
         1f:11:5d:9a:0a:c2:f9:68:15:af:5f:a6:72:27:c3:76:7c:8b:
         f3:ea:2f:a9:e2:ee:01:24:53:7d:f5:1a:26:73:14:9b:62:eb:
         87:3a:84:9e:85:85:bd:64:30:5b:99:2b:54:94:30:df:00:3f:
         be:ba:f9:bb:6c:2d:b8:e8:3f:8b:3f:0e:e3:48:b6:06:d9:e0:
         01:e7:7d:90:db:e5:d9:42:66:55:c3:57:a8:9b:ad:90:90:a1:
         0f:3a:7a:82:f3:3e:2f:4c:49:5d:ea:49:a3:80:37:47:7d:28:
         e4:62:ea:99:f0:6a:b0:45:08:6f:02:1d:3c:4d:d7:ff:da:24:
         95:81:4c:0e:9e:96:0f:36:ec:3d:cf:7e:35:37:97:a8:ab:ec:
         13:56:cd:fc:c9:9f:d0:c0:d4:e7:ce:75:c2:eb:b0:71:b2:50:
         cc:ce:7f:05:6a:85:dc:04:13:1d:f7:5d:bc:4a:58:a4:8e:99:
         bd:77:6a:62:ad:1d:0b:00:70:93:e9:34:80:d3:dd:4a:6d:87:
         2f:5a:80:fe:aa:52:2b:10:10:e5:b7:0e:ff:da:ed:c2:f2:23:
         4e:1d:aa:3b
```


[man_req]: https://linux.die.net/man/1/req
[idioms]: openssl.md
[key]: key.md
[conf]: conf.md
