# CSR Config

## Ehhh...

These files are pretty optional, especially as many of our certificates will
have a CSR generated on the device. However...

## Usage

It can be convinent to have CSR config file to create a CSR that includes the
subject alternate names (SANs) and settings that will be used when the request
is signed. Use `template.conf` as a template. The only fields you should change
are:

* CN: This is the FQDN Common Name of the server
* DNS.#: SANs

Using a config file makes it easier to put the correct SANs in the web form
when requesting certs by doing something like:

```bash
openssl req -in <CSR> -noout -text \
    | grep DNS \
    | sed 's/ *DNS://g'
```
