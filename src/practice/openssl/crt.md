# Certificates

## File Format

Follow these guidelines when adding certificates:

* **Use the CN as the file name, and crt as the extension.** E.g., if your
  CN is 'example.nis.vt.edu', then the file name should be
  'example.nis.vt.edu.crt'.

* **Use PEM format.** Using a format that can be edited in a text editor
  simplifies the workflow.

* **Include comments.** This is convenient when buiding a chain. In order,
  the following comments should be included, before the certificate:

    1. Subject
    2. Issuer
    3. Dates
    4. Modulus

* **Terminate the last line with an `EOL`.** The certificate downloaded from
  certs.it.vt.edu omits this, which breaks `cat`-ing together a chain.

tldr:

```bash
openssl x509 -in downloaded.pem -subject -issuer -dates -modulus > CN.crt
```

## Creating a chained file

### X509

Just `cat` the files together. The order should be:

1. Leaf certificate
2. Private Key
3. Intermediate certs
4. Root Certs

See the README in the key directory for notes on re-encrypting the private key!
Attaching the PGP encrypted key will _not_ work.

```bash
cat \
  CN.crt \
  CN.rsa \
  InCommon_RSA_Server_CA.crt \
  USERTrust_RSA_Certification_Authority.crt \
  > CN_chain.pem
```

### PKCS12

PKCS12, or p12, is commonly used by Microsoft servers and a few other things.
It packages together the cert, key, and the chain into a single structure.

```bash
openssl pkcs12 \
  -export \
  -in CN.crt \
  -certfile <(cat \
    InCommon_RSA_Server_CA.crt \
    USERTrust_RSA_Certification_Authority.crt) \
  -inkey <(gpg -qd CN.rsa.asc) \
  -passout file:<(gpg -qd pwfile.pgp) \
  -out CN.p12 \
  -aes256
```

## Tips

The server should always send the full chain, not just the leaf certificate.
You can verify this with the `openssl s_client` command.

* Use `-connect <server>:<port>` to specify what server to connect to.
* Pass the string "QUIT" in on stdin to avoid staying in interactive mode.
* Use `-verify_return_error` to validate the chain (this will kill the
  connection, which may leave out useful information).
* Use `-CAfile <root cert>` to specify what root CA should be used to verify
  the certificate chain.

Putting it all together:

```bash
openssl s_client \
    -connect example.nis.vt.edu:443 \
    -verify_return_error \
    -CAfile crt/chain/USERTrust_RSA_Certfication_Authority.crt \
    <<< "QUIT"
```
