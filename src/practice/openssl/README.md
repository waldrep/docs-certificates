# OpenSSL idioms

OpenSSL is a cryptography toolkit implementing the Secure Sockets Layer (SSL
v2/v3) and Transport Layer Security (TLS v1) network protocols and related
cryptography standards required by them. [[1][man]]

The general form is `openssl command [ command_opts ]`.

## Default behavior

Note that there are no unnamed options.
That is, all options have the form `-option_name value`, and never just `value`.
This includes input and output filenames, which is commonly an unnamed argument
in other cli tools.

The default input is stdin, and the default output is stdout.

The default action is to out put the PEM encoded version of whatever was just
read or created.


## Common options

There are some options that are commonly used by commands.
Here are a few:
* `-in`: specify an input file for the thing
* `-new`: create a thing
* `-out`: output file for the thing
* `-noout`: do not output the PEM encoding of the thing.
  This is particularly useful when you want to see a human readable form of the
  thing and do not care about the machine readable form.
* `-text`: display a human readable form of the thing


[man]: https://linux.die.net/man/1/openssl
