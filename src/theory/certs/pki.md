# PKI and Chain of Trust

When you read the [certificate overview](overview.md), you may have been
wondering, "OK, I trust a certificate because it was signed by something else...
but how to I trust the thing that signed the certificate?"
This page seeks to answer that.

## Root of trust

A root of trust, or trust anchor, is something that is inherently trusted.
You can then use this anchor to establish trust with another entity.

These private keys are _very_ sensitive information, and are usually stored
on encrypted, air-gapped media.

## Root Certificate Authority (CA)

A root certificate or CA is a self-signed certificate that acts as a trust
anchor.
Note that the strength of the self-signature is irrelevant, as the certificate
is inherently trusted.
The strength of the signatures made by the root CA on other certificates is
obviously still critical.

## Chain of trust

Trust flows from a root certificate to the leaf certificate.
The leaf certificate is what is used by clients and servers to identify
themselves.

Once you trust a root certificate, it can be used to sign other certificates.
Because you trust the root certificate, you also trust the certificates it has
signed.

Usually, the root CA signs an intermediate CA, which signs the leaf certificate.
There may be multiple intermediate CAs between the root and the leaf.
Additionally, a root CA may directly sign multiple intermediate CAs.

Having intermediate CAs allows new certificates to be signed, while keeping the
root private key offline.

Additionally, with multiple "branches" of trust (when a CA signs multiple
intermediates), there is a separation of risk.

## Public Key Infrastructure (PKI)

A public key infrastructure (PKI) is the whole system of machinery around a CA.
It is what distributes intermediate CAs, signs certificates, etc.

## Distributing a custom root CA

If you are not using certificates for HTTPS, you will likely need a way to
distribute the root CA.

This can be done by having the on-boarding software install it automatically, or
posting it on a secure and trusted site.

Keep in mind this is the user's trust anchor.
If possible, restrict its usage to your application.
Make sure your users can verify the _integrity_ and _authenticity_ of this
certificate.
