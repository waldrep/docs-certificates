# The CA/Browser forum

## What is the CA/Browser forum?

The CA/Browser forum is a consortium that writes the industry guidelines on
issuing and management of certificates.
Unsurprisingly, the forum consists of root certificate authorities (the
organizations behind the actual root certificates e.g., GoDaddy and GlobalSign)
and browser makers (Google, Mozilla, etc).

These guidelines include things such as:
* Types of keys
* Minimum key size
* Serial number issuance
* How long a certificate is valid for

## Common certificate store

Most browsers ship with, or leverage operating systems' set of, root CAs of the
members of the CA/Browser forum.
This gives a common set of root CAs that a website administrator can expect
browsers to have.

This set of root CAs is _usually_ limited to HTTPS use.
