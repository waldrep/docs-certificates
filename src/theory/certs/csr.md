# Getting certificates signed

## Basic steps

1. Create a certificate signing request
1. Send the CSR to the CA
1. Get the certificate from the CA


## Create a Certificate Signing Request
To generate a Certificate Signing Request (CSR), you will need a few different
things:
1. A key
1. Know what metadata you want in the CSR.
   Namely, you will need:
   * The Common Name (CN)
   * Subject Alternate Names (SAN or AN)

As simple example, suppose you have a standalone box, which is reachable at
`example.nis.vt.edu`.
Because our DNS crew rocks, it is also reachable at `example.nis.ipv4.vt.edu`
(which resolves only to the legacy IP address) and `example.nis.ipv6.vt.edu`
(which resolves only to the IPv6 address).
The CN would be `example.nis.vt.edu` and the SANs would be
`example.nis.ipv4.vt.edu` and `example.nis.ipv6.vt.edu`.

Many applications will generate the CSR and key for you. In this case, you
usually cannot export the private key.

The CSR itself contains the public key and some of the metadata that will be
included on the certificate, and itself signed with the private key, mostly as
a sanity check.

## Send the CSR to the CA
The CA will add some metadata of its own (such as Issuer, valid dates, and
serial number), and will likely override some set in the CSR (such as
organizational info).
The trust comes from the CA, so it has the last say.
As a convenience, the CA may provide a way to input metadata to me included on
the certificate, such as SANs.

The CA will also do some kind of verification that the certificate is legit.
This could be as simple as having logged in with an account that can request
any certificate within a domain.
The ACME protocol is another common way to verify domain ownership.

With that public key from the CSR and the validated metadata provided, the CA
bundles it appropriately and signs it all, creating the certificate.


## Get the certificate from the CA
The CA then provides a link or some way to obtain the certificate.
Download it and install it with the private key as needed.


# How is this process secured?
So which bits here are sensitive?
Do we have to be have to be careful with the CSR or the returned certificate?

_The only sensitive information is the private key, which is never shared with
the CA._
The certificate itself is what is presented to the other party (e.g., the
browser) to prove ownership of the key and domain.
That means if someone other than the requester gets the link to download the
certificate, it is the same thing they get by using the service the certificate
supports.
Nothing sensitive there.

The only other sensitive part is the domain verification step.
If an attacker can mimic that verification (e.g., compromised account to request
a certificate), then the attacker can request a certificate for your service and
their key.
The CA has a record of all certificates issued, so this can be verified.
