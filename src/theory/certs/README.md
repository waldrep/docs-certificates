# Certificates

## What is a certificate?

At its core, a certificate is a public key bundled with some metadata, and a
signature on the bundle.

The public key could be one for encryption or, more commonly, to verify a
signature of the associated private key.

The goal of a certificate is to establish authenticated identity about who owns
the private key.
This is usually used for channel elevation or client authentication.

## Certificate strength

Obviously, a certificate is only as good as the key.
If an attacker can derive the private key from the public key, then any
operations done with the private key are broken, and the certificate doesn't
gain us anything.

As with most signatures, the message (in this case, the key and metadata bundle)
is hashed, and the hash itself is signed.
This means the signature is only as strong as the _second preimage resistance_
of the hashing algorithm.

## Metadata

The specifics of the included metadata is very application dependent.
This is a list of some commonly used metadata:

* **subject**: Who owns this certificate.
  This usually includes:
  * _Common Name_ (CN): Usually an FQDN for server certificates and a username
    for client certificates.
  * Organizational information: several fields containing the name of the
    organization, organizational unit, and the associated postal address.
* **issuer**: CN of the certificate that signed this certificate.
* **dates**: A start date and an end date for when the certificate is valid.
* **key usage**: What is this key allowed to do?
  Common uses include:
  * Web server (HTTPS)
  * Signing email
  * Client authentication
  * Signing other certificates
* **Issuer metadata**: Information on how to identify the signing certificate
  and where to find it.
