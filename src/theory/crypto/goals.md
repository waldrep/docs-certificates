# Cryptographic Goals

## Privacy

Knowing that only the intended recipient(s) can read a message.

## Anonymity

Hiding who you are or who you are talking to.

## Integrity

Knowing that a message was not altered.

## Authenticity

Knowing who the message came from.

## Authorization

What an entity is allowed to do.

## Channel elevation

Establishing a secure channel over an insecure channel e.g., HTTPS over the
Internet.
